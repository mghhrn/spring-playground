create table student (
    id int8 primary key,
    first_name varchar,
    national_id int8
);

insert into student values (1, 'Mehdi', '23524');
insert into student values (2, 'Hasan', '5898348');
